namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TestAnswers",
                c => new
                    {
                        TestAnswerId = c.Int(nullable: false, identity: true),
                        TestQuestionId = c.Int(nullable: false),
                        Instance = c.String(),
                        IsCorrect = c.Boolean(nullable: false),
                        Guid = c.String(),
                    })
                .PrimaryKey(t => t.TestAnswerId)
                .ForeignKey("dbo.TestQuestions", t => t.TestQuestionId, cascadeDelete: true)
                .Index(t => t.TestQuestionId);
            
            CreateTable(
                "dbo.TestQuestions",
                c => new
                    {
                        TestQuestionId = c.Int(nullable: false, identity: true),
                        TestId = c.Int(nullable: false),
                        Instance = c.String(),
                        Hint = c.String(),
                        Guid = c.String(),
                    })
                .PrimaryKey(t => t.TestQuestionId)
                .ForeignKey("dbo.Tests", t => t.TestId, cascadeDelete: true)
                .Index(t => t.TestId);
            
            CreateTable(
                "dbo.Tests",
                c => new
                    {
                        TestId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        TestTimeLimit = c.Time(precision: 7),
                        QuestionTimeLimit = c.Time(precision: 7),
                        Guid = c.String(),
                    })
                .PrimaryKey(t => t.TestId);
            
            CreateTable(
                "dbo.TestingResults",
                c => new
                    {
                        TestingResultId = c.Int(nullable: false, identity: true),
                        Guid = c.String(),
                        TestingStartDateTime = c.DateTime(nullable: false),
                        TestingEndDateTime = c.DateTime(nullable: false),
                        Duration = c.Time(nullable: false, precision: 7),
                        Interviewee = c.String(),
                        QuestionTried = c.Int(nullable: false),
                        Score = c.Double(nullable: false),
                        AttemptGuid = c.String(),
                        IsValid = c.Boolean(nullable: false),
                        Test_TestId = c.Int(),
                        TestingUrl_TestingUrlId = c.Int(),
                    })
                .PrimaryKey(t => t.TestingResultId)
                .ForeignKey("dbo.Tests", t => t.Test_TestId)
                .ForeignKey("dbo.TestingUrls", t => t.TestingUrl_TestingUrlId)
                .Index(t => t.Test_TestId)
                .Index(t => t.TestingUrl_TestingUrlId);
            
            CreateTable(
                "dbo.TestingResultAnswers",
                c => new
                    {
                        TestingResultAnswerId = c.Int(nullable: false, identity: true),
                        TestingResultId = c.Int(nullable: false),
                        TestQuestionId = c.Int(nullable: false),
                        TestAnswersSelected = c.String(),
                    })
                .PrimaryKey(t => t.TestingResultAnswerId)
                .ForeignKey("dbo.TestingResults", t => t.TestingResultId, cascadeDelete: true)
                .ForeignKey("dbo.TestQuestions", t => t.TestQuestionId, cascadeDelete: true)
                .Index(t => t.TestingResultId)
                .Index(t => t.TestQuestionId);
            
            CreateTable(
                "dbo.TestingUrls",
                c => new
                    {
                        TestingUrlId = c.Int(nullable: false, identity: true),
                        TestId = c.Int(nullable: false),
                        Interviewee = c.String(),
                        AllowedStartDate = c.DateTime(),
                        AllowedEndDate = c.DateTime(),
                        NumberOfRuns = c.Int(nullable: false),
                        Guid = c.String(),
                    })
                .PrimaryKey(t => t.TestingUrlId)
                .ForeignKey("dbo.Tests", t => t.TestId, cascadeDelete: true)
                .Index(t => t.TestId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TestAnswers", "TestQuestionId", "dbo.TestQuestions");
            DropForeignKey("dbo.TestQuestions", "TestId", "dbo.Tests");
            DropForeignKey("dbo.TestingResults", "TestingUrl_TestingUrlId", "dbo.TestingUrls");
            DropForeignKey("dbo.TestingUrls", "TestId", "dbo.Tests");
            DropForeignKey("dbo.TestingResultAnswers", "TestQuestionId", "dbo.TestQuestions");
            DropForeignKey("dbo.TestingResultAnswers", "TestingResultId", "dbo.TestingResults");
            DropForeignKey("dbo.TestingResults", "Test_TestId", "dbo.Tests");
            DropIndex("dbo.TestingUrls", new[] { "TestId" });
            DropIndex("dbo.TestingResultAnswers", new[] { "TestQuestionId" });
            DropIndex("dbo.TestingResultAnswers", new[] { "TestingResultId" });
            DropIndex("dbo.TestingResults", new[] { "TestingUrl_TestingUrlId" });
            DropIndex("dbo.TestingResults", new[] { "Test_TestId" });
            DropIndex("dbo.TestQuestions", new[] { "TestId" });
            DropIndex("dbo.TestAnswers", new[] { "TestQuestionId" });
            DropTable("dbo.Users");
            DropTable("dbo.TestingUrls");
            DropTable("dbo.TestingResultAnswers");
            DropTable("dbo.TestingResults");
            DropTable("dbo.Tests");
            DropTable("dbo.TestQuestions");
            DropTable("dbo.TestAnswers");
        }
    }
}
