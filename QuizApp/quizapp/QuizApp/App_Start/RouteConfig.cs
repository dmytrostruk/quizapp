﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace QuizApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "tests",
                url: "tests",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "results",
                url: "results",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "urls",
                url: "urls",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "home",
                url: "home",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "quizget",
                url: "Quiz/GetInfoAndStartTest",
                defaults: new { controller = "Quiz", action = "GetInfoAndStartTest" }
            );

            routes.MapRoute(
                name: "quizfinish",
                url: "Quiz/FinishTest",
                defaults: new { controller = "Quiz", action = "FinishTest" }
            );

            routes.MapRoute(
                name: "quiz",
                url: "quiz/{guid}",
                defaults: new { controller = "Quiz", action = "Quiz", guid = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "CSV",
                url: "CSV/{testGuid}",
                defaults: new {controller = "Admin", action = "GetResultsForTestCsv" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "Account", action = "Login" }
            );
        }
    }
}
