﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using ModelClasses.Entities.Testing;
using ModelClasses.Entities.TestParts;
using QuizApp.ViewModel;
using QuizApp.ViewModel.Managing;
using QuizApp.ViewModel.Mapping;
using Services;

namespace QuizApp.Controllers
{
    [Authorize]
    public class ApilikeController : Controller
    {
        private readonly IGetInfoService _getInfoService;
        private readonly ILowLevelTestManagementService _lowLevelTestManagementService;
        private readonly IHighLevelTestManagementService _highLevelTestManagementService;

        private readonly IMapper _mapper;
        private readonly IAdvancedMapper _advancedMapper;

        public ApilikeController(IGetInfoService getInfoService,
            ILowLevelTestManagementService lowLevelTestManagementService,
            IHighLevelTestManagementService highLevelTestManagementService, IMapper mapper,
            IAdvancedMapper advancedMapper)
        {
            _getInfoService = getInfoService;
            _lowLevelTestManagementService = lowLevelTestManagementService;
            _highLevelTestManagementService = highLevelTestManagementService;
            _mapper = mapper;
            _advancedMapper = advancedMapper;
        }

        [HttpGet]
        public JsonResult GetAnswersByQuestionGuid(string questionGuid)
        {
            var answerViewModelList = _getInfoService
                .GetQuestionByGuid(questionGuid)
                ?.TestAnswers
                .Select(a => _mapper.Map<AnswerViewModel>(a))
                .ToList();

            return Json(answerViewModelList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CreateAnswer(string questionGuid, AnswerViewModel answer)
        {
            var testAnswer = _mapper.Map<TestAnswer>(answer);
            _lowLevelTestManagementService.CreateAnswerForQuestion(questionGuid, testAnswer);

            if(testAnswer.Guid != null)
            {
                answer.Guid = testAnswer.Guid;
            }

            return Json(answer, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveAnswer(string answerGuid)
        {
            _lowLevelTestManagementService.RemoveAnswer(answerGuid);
            return Json(answerGuid, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetQuestionsByTestGuid(string testGuid)
        {
            var questionViewModelList = _getInfoService
                .GetTestByGuid(testGuid)
                ?.TestQuestions
                .Select(q => _advancedMapper.MapTestQuestion(q))
                .ToList();

            return Json(questionViewModelList, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult CreateQuestion(string testGuid, QuestionViewModel question)
        {
            var testQuestion = _mapper.Map<TestQuestion>(question);
            _lowLevelTestManagementService.CreateQuestionForTest(testGuid, testQuestion);

            if(testQuestion.Guid != null)
            {
                question.Guid = testQuestion.Guid;
            }

            return Json(question, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveQuestion(string testGuid, string questionGuid)
        {
            _lowLevelTestManagementService.RemoveQuestion(questionGuid);
            return Json(questionGuid, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateQuestion(string questionGuid, QuestionViewModel question)
        {
            var testQuestion = _mapper.Map<TestQuestion>(question);
            _lowLevelTestManagementService.UpdateQuestion(questionGuid, testQuestion);
            return Json(testQuestion, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateTest(TestViewModel test)
        {
            var testFromDomain = _advancedMapper.MapTestViewModel(test);
            _highLevelTestManagementService.CreateTest(testFromDomain);
            return Json(testFromDomain, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateTest(string testGuid, TestViewModel test)
        {
            var testFromDomain = _advancedMapper.MapTestViewModel(test);
            _highLevelTestManagementService.UpdateTest(testGuid, testFromDomain);
            return Json(testFromDomain, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveTest(string testGuid)
        {
            _highLevelTestManagementService.RemoveTest(testGuid);
            return Json(testGuid, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateTestingUrl(TestingUrlViewModel testingUrl)
        {
            var testUrlDomain = _advancedMapper.MapTestingUrlViewModel(testingUrl);
            _highLevelTestManagementService.CreateTestingUrl(testUrlDomain);

            testingUrl.Guid = testUrlDomain.Guid;
            testingUrl.TestName = testUrlDomain.Test.Name;

            return Json(testingUrl, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveTestingUrl(string testingUrlGuid)
        {
            _highLevelTestManagementService.RemoveTestingUrl(testingUrlGuid);
            return Json(testingUrlGuid, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult RemoveTestingResult(string testingResultGuid)
        {
            _highLevelTestManagementService.RemoveTestingResult(testingResultGuid);
            return Json(testingResultGuid, JsonRequestBehavior.AllowGet);
        }
    }
}