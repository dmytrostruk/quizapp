"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
var error_module_1 = require("./Components/Error/error.module");
var platform = platform_browser_dynamic_1.platformBrowserDynamic();
platform.bootstrapModule(error_module_1.ErrorModule);
//# sourceMappingURL=error.js.map