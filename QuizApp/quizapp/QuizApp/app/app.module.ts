﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { ResultsComponent } from './Components/Results/results.component';
import { TestsComponent } from './Components/Tests/tests.component';
import { UrlsComponent } from './Components/Urls/urls.component';
import { TestComponent } from './Components/Test/test.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        AppComponent,
        ResultsComponent,
        TestsComponent,
        UrlsComponent,
        TestComponent,
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule { }