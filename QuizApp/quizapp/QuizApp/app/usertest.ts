﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { UserTestModule } from './Components/UserTest/usertest.module';
const platform = platformBrowserDynamic();
platform.bootstrapModule(UserTestModule);