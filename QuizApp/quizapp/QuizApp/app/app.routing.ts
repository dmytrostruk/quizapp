﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ResultsComponent } from './Components/Results/results.component';
import { TestsComponent } from './Components/Tests/tests.component';
import { UrlsComponent } from './Components/Urls/urls.component';

import { testRoutes } from './Components/Tests/tests.routing';
import { urlsRoutes } from './Components/Urls/urls.routing';

const appRoutes: Routes = [
    { path: '', redirectTo: '/tests', pathMatch: 'full' },
    { path: 'tests', component: TestsComponent, },
    { path: 'urls', component: UrlsComponent },
    { path: 'results', component: ResultsComponent },
    ...testRoutes,
    ...urlsRoutes
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);