﻿import { Component } from "@angular/core";
import { TestService } from './Services/test.service';
import { QuestionService } from './Services/question.service';
import { AnswerService } from './Services/answer.service';
import { UrlsService } from './Services/urls.service';
import { ResultsService } from './Services/results.service';

@Component({
    moduleId: module.id,
    selector: "my-app",
    templateUrl: 'app.component.html',
    providers: [
        TestService,
        QuestionService,
        AnswerService,
        UrlsService,
        ResultsService
    ]
})

export class AppComponent {

    logOut() {
        window.location.href = 'Account/LogOff';
    }

}