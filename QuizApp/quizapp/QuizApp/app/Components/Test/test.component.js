"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var router_2 = require("@angular/router");
var test_service_1 = require("../../Services/test.service");
var question_service_1 = require("../../Services/question.service");
var answer_service_1 = require("../../Services/answer.service");
var TestComponent = (function () {
    function TestComponent(formBuilder, _testService, _questionService, _answerService, activatedRoute, router) {
        this.formBuilder = formBuilder;
        this._testService = _testService;
        this._questionService = _questionService;
        this._answerService = _answerService;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.updatedTest = {
            Guid: "",
            Name: "",
            Description: "",
            TestTimeLimit: "00:00:00",
            QuestionTimeLimit: "00:00:00"
        };
        this.updatedQuestion = {
            Guid: "",
            Instance: "",
            Hint: "",
            Answers: null
        };
        this.newQuestion = {
            Guid: "",
            Instance: "",
            Hint: "",
            Answers: null
        };
        this.editTestForm = this.formBuilder.group({
            name: ['', forms_1.Validators.required],
            description: ['', forms_1.Validators.required]
        });
        this.addQuestionForm = this.formBuilder.group({
            instance: ['', forms_1.Validators.required],
            hint: ['', forms_1.Validators.required]
        });
        this.editQuestionForm = this.formBuilder.group({
            instance: ['', forms_1.Validators.required],
            hint: ['', forms_1.Validators.required]
        });
        this.addAnswerForm = this.formBuilder.group({
            instance: ['', forms_1.Validators.required],
            correct: false
        });
    }
    TestComponent.prototype.ngOnInit = function () {
        this.test = this._testService.currentTest;
        this.editTestForm.setValue({ name: this.test.Name, description: this.test.Description });
        this.getTestQuestions();
    };
    TestComponent.prototype.changeRoute = function () {
        this.router.navigate(["tests"]);
    };
    TestComponent.prototype.removeTest = function () {
        var _this = this;
        this._testService.removeTest(this.test.Guid).subscribe(function (item) {
            if (item != null) {
                _this.changeRoute();
            }
        });
    };
    TestComponent.prototype.editTest = function () {
        var _this = this;
        this.updatedTest.Guid = this.test.Guid;
        this.updatedTest.Name = this.editTestForm.get('name').value;
        this.updatedTest.Description = this.editTestForm.get('description').value;
        this._testService.editTest(this.test.Guid, this.updatedTest)
            .subscribe(function (test) {
            _this.test = test;
        });
    };
    TestComponent.prototype.getTestQuestions = function () {
        var _this = this;
        this._questionService.getQuestionsByTestGuid(this.test.Guid)
            .subscribe(function (questions) {
            _this.questions = questions;
        });
    };
    TestComponent.prototype.addTestQuestion = function () {
        var _this = this;
        this.newQuestion.Instance = this.addQuestionForm.get('instance').value;
        this.newQuestion.Hint = this.addQuestionForm.get('hint').value;
        this._questionService.addQuestion(this.test.Guid, this.newQuestion)
            .subscribe(function (question) {
            _this.questions.push(question);
        });
    };
    TestComponent.prototype.removeTestQuestion = function () {
        var _this = this;
        this._questionService.removeQuestion(this.test.Guid, this.selectedQuestion.Guid)
            .subscribe(function (guid) {
            var index = _this.questions.indexOf(_this.selectedQuestion, 0);
            if (index > -1) {
                _this.questions.splice(index, 1);
            }
        });
    };
    TestComponent.prototype.selectQuestion = function (question) {
        this.selectedQuestion = question;
        this.selectedAnswers = this.selectedQuestion.Answers;
        this.editQuestionForm.setValue({ instance: this.selectedQuestion.Instance, hint: this.selectedQuestion.Hint });
    };
    TestComponent.prototype.editTestQuestion = function () {
        var _this = this;
        this.updatedQuestion.Guid = this.selectedQuestion.Guid;
        this.updatedQuestion.Answers = this.selectedQuestion.Answers;
        this.updatedQuestion.Instance = this.editQuestionForm.get('instance').value;
        this.updatedQuestion.Hint = this.editQuestionForm.get('hint').value;
        this._questionService.editQuestion(this.updatedQuestion.Guid, this.updatedQuestion)
            .subscribe(function (question) {
            _this.selectedQuestion.Instance = question.Instance;
            _this.selectedQuestion.Hint = question.Hint;
        });
    };
    TestComponent.prototype.addAnswer = function () {
        var _this = this;
        var newAnswer = {
            Guid: '',
            Instance: this.addAnswerForm.get('instance').value,
            IsCorrect: this.addAnswerForm.get('correct').value
        };
        this._answerService.addNewAnswer(this.selectedQuestion.Guid, newAnswer)
            .subscribe(function (answer) {
            _this.selectedQuestion.Answers.push(answer);
        });
    };
    TestComponent.prototype.onChangeSelectedAnswer = function (guid) {
        this.selectedAnswerGuid = guid;
    };
    TestComponent.prototype.deleteAnswer = function () {
        var _this = this;
        this._answerService.deleteAnswer(this.selectedAnswerGuid)
            .subscribe(function (guid) {
            var index = -1;
            for (var i = 0; i < _this.selectedAnswers.length; i++) {
                if (_this.selectedAnswers[i].Guid === guid) {
                    index = i;
                }
            }
            if (index > -1) {
                _this.selectedAnswers.splice(index, 1);
            }
            var option = document.getElementById('default-list-value');
            option.selected = true;
            _this.selectedAnswerGuid = null;
        });
    };
    TestComponent.prototype.downloadCSV = function () {
        window.location.href = "Admin/GetResultsForTestCsv?testGuid=" + this.test.Guid;
    };
    return TestComponent;
}());
TestComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "test",
        templateUrl: 'test.component.html',
        styleUrls: ['test.component.css'],
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        test_service_1.TestService,
        question_service_1.QuestionService,
        answer_service_1.AnswerService,
        router_2.ActivatedRoute,
        router_1.Router])
], TestComponent);
exports.TestComponent = TestComponent;
//# sourceMappingURL=test.component.js.map