﻿import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Test } from '../../Models/test.model';
import { Question } from '../../Models/question.model';
import { Answer } from '../../Models/answer.model';

import { TestService } from '../../Services/test.service';
import { QuestionService } from '../../Services/question.service';
import { AnswerService } from '../../Services/answer.service';

@Component({
    moduleId: module.id,
    selector: "test",
    templateUrl: 'test.component.html',
    styleUrls: ['test.component.css'],
})

export class TestComponent implements OnInit {
    private testGuid: string;
    private sub: any;

    private test: Test;
    private questions: Question[];
    private selectedAnswers: Answer[];
    private selectedAnswerGuid: string;

    private updatedTest: Test = {
        Guid: "",
        Name: "",
        Description: "",
        TestTimeLimit: "00:00:00",
        QuestionTimeLimit: "00:00:00"
    };

    private updatedQuestion: Question = {
        Guid: "",
        Instance: "",
        Hint: "",
        Answers: null
    }

    private newQuestion: Question = {
        Guid: "",
        Instance: "",
        Hint: "",
        Answers: null
    };

    private selectedQuestion: Question;

    private editTestForm: FormGroup = this.formBuilder.group({
        name: ['', Validators.required],
        description: ['', Validators.required]
    })

    private addQuestionForm: FormGroup = this.formBuilder.group({
        instance: ['', Validators.required],
        hint: ['', Validators.required]
    })

    private editQuestionForm: FormGroup = this.formBuilder.group({
        instance: ['', Validators.required],
        hint: ['', Validators.required]
    })

    private addAnswerForm: FormGroup = this.formBuilder.group({
        instance: ['', Validators.required],
        correct: false
    })

    constructor(
        private formBuilder: FormBuilder,
        private _testService: TestService,
        private _questionService: QuestionService,
        private _answerService: AnswerService,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.test = this._testService.currentTest;
        this.editTestForm.setValue({ name: this.test.Name, description: this.test.Description });
        this.getTestQuestions();
    }

    changeRoute() {
        this.router.navigate(["tests"]);
    }

    removeTest() {
        this._testService.removeTest(this.test.Guid).subscribe(item => {
            if (item != null) {
                this.changeRoute();
            }
        });
    }

    editTest() {
        this.updatedTest.Guid = this.test.Guid;
        this.updatedTest.Name = this.editTestForm.get('name').value;
        this.updatedTest.Description = this.editTestForm.get('description').value;

        this._testService.editTest(this.test.Guid, this.updatedTest)
            .subscribe(test => {
                this.test = test;
            });
    }

    getTestQuestions() {
        this._questionService.getQuestionsByTestGuid(this.test.Guid)
            .subscribe(questions => {
                this.questions = questions;
            });
    }

    addTestQuestion() {
        this.newQuestion.Instance = this.addQuestionForm.get('instance').value;
        this.newQuestion.Hint = this.addQuestionForm.get('hint').value;

        this._questionService.addQuestion(this.test.Guid, this.newQuestion)
            .subscribe(question => {
                this.questions.push(question);
            });
    }

    removeTestQuestion() {
        this._questionService.removeQuestion(this.test.Guid, this.selectedQuestion.Guid)
            .subscribe(guid => {
                let index = this.questions.indexOf(this.selectedQuestion, 0);
                if (index > -1) {
                    this.questions.splice(index, 1);
                }
            });
    }

    selectQuestion(question: Question) {
        this.selectedQuestion = question;
        this.selectedAnswers = this.selectedQuestion.Answers;
        this.editQuestionForm.setValue({ instance: this.selectedQuestion.Instance, hint: this.selectedQuestion.Hint });
    }

    editTestQuestion() {
        this.updatedQuestion.Guid = this.selectedQuestion.Guid;
        this.updatedQuestion.Answers = this.selectedQuestion.Answers;

        this.updatedQuestion.Instance = this.editQuestionForm.get('instance').value;
        this.updatedQuestion.Hint = this.editQuestionForm.get('hint').value;

        this._questionService.editQuestion(this.updatedQuestion.Guid, this.updatedQuestion)
            .subscribe(question => {
                this.selectedQuestion.Instance = question.Instance;
                this.selectedQuestion.Hint = question.Hint;
            });
    }

    addAnswer() {
        let newAnswer: Answer = {
            Guid: '',
            Instance: this.addAnswerForm.get('instance').value,
            IsCorrect: this.addAnswerForm.get('correct').value
        }

        this._answerService.addNewAnswer(this.selectedQuestion.Guid, newAnswer)
            .subscribe(answer => {
                this.selectedQuestion.Answers.push(answer);
            });
    }

    onChangeSelectedAnswer(guid) {
        this.selectedAnswerGuid = guid;
    }

    deleteAnswer() {
        this._answerService.deleteAnswer(this.selectedAnswerGuid)
            .subscribe(guid => {
                let index = -1;
                for (var i = 0; i < this.selectedAnswers.length; i++) {
                    if (this.selectedAnswers[i].Guid === guid) {
                        index = i;
                    }
                }

                if (index > -1) {
                    this.selectedAnswers.splice(index, 1);
                }

                var option = <HTMLSelectElement>document.getElementById('default-list-value');
                option.selected = true;
                this.selectedAnswerGuid = null;
            });
    }

    downloadCSV() {
        window.location.href = "Admin/GetResultsForTestCsv?testGuid=" + this.test.Guid;
    }
}