"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var results_service_1 = require("../../Services/results.service");
var ResultsComponent = (function () {
    function ResultsComponent(_resultsService) {
        this._resultsService = _resultsService;
    }
    ResultsComponent.prototype.ngOnInit = function () {
        this.getResults();
    };
    ResultsComponent.prototype.getResults = function () {
        var _this = this;
        this._resultsService.getResults()
            .subscribe(function (results) {
            _this.results = results;
        });
    };
    ResultsComponent.prototype.selectResult = function (selectedResult, index) {
        this.selectedResult = selectedResult;
        this.selectedRow = index;
    };
    ResultsComponent.prototype.removeResult = function () {
        var _this = this;
        this._resultsService.removeTestingUrl(this.selectedResult.Guid)
            .subscribe(function (urlGuid) {
            var index = _this.results.indexOf(_this.selectedResult, 0);
            if (index > -1) {
                _this.results.splice(index, 1);
            }
            _this.selectedResult = null;
            _this.selectedRow = -1;
        });
    };
    return ResultsComponent;
}());
ResultsComponent = __decorate([
    core_1.Component({
        selector: "results",
        moduleId: module.id,
        templateUrl: 'results.component.html',
        styleUrls: ['results.component.css'],
    }),
    __metadata("design:paramtypes", [results_service_1.ResultsService])
], ResultsComponent);
exports.ResultsComponent = ResultsComponent;
//# sourceMappingURL=results.component.js.map