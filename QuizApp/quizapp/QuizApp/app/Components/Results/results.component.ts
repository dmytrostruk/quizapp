﻿import { Component, OnInit, Input, Output } from "@angular/core";
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import { Result } from '../../Models/result.model';
import { ResultsService } from '../../Services/results.service';

@Component({
    selector: "results",
    moduleId: module.id,
    templateUrl: 'results.component.html',
    styleUrls: ['results.component.css'],
})

export class ResultsComponent implements OnInit {

    private results: Result[];
    private selectedResult: Result;
    private selectedRow: number;
  
    constructor(
        private _resultsService: ResultsService
    ) { }

    ngOnInit() {
        this.getResults();
    }

    getResults() {
        this._resultsService.getResults()
            .subscribe(results => {
                this.results = results;
            });
    }

    selectResult(selectedResult: Result, index: number) {
        this.selectedResult = selectedResult;
        this.selectedRow = index;
    }

    removeResult() {
        this._resultsService.removeTestingUrl(this.selectedResult.Guid)
            .subscribe(urlGuid => {
                let index = this.results.indexOf(this.selectedResult, 0);
                if (index > -1) {
                    this.results.splice(index, 1);
                }

                this.selectedResult = null;
                this.selectedRow = -1;
            });
    }
}