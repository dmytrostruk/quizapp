﻿import { Routes } from '@angular/router';

import { TestComponent } from '../Test/test.component';
import { TestsComponent } from './tests.component';

export const testRoutes: Routes = [
    { path: 'tests', component: TestsComponent },
    { path: 'testpage', component: TestComponent, pathMatch: 'prefix' }
];