"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var test_service_1 = require("../../Services/test.service");
var TestsComponent = (function () {
    function TestsComponent(_testService, formBuilder, router) {
        this._testService = _testService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.addTestForm = this.formBuilder.group({
            name: ['', forms_1.Validators.required],
            description: ['', forms_1.Validators.required],
            hours: [0, forms_1.Validators.required],
            minutes: [0, forms_1.Validators.required],
            seconds: [0, forms_1.Validators.required]
        });
        this.newTest = {
            Guid: "",
            Name: "",
            Description: "",
            TestTimeLimit: "00:00:00",
            QuestionTimeLimit: "00:00:00"
        };
    }
    TestsComponent.prototype.ngOnInit = function () {
        this.getTests();
    };
    TestsComponent.prototype.getTests = function () {
        var _this = this;
        this._testService.getTests().subscribe(function (tests) {
            _this.tests = tests;
        });
    };
    TestsComponent.prototype.addTest = function () {
        var _this = this;
        this.newTest.Name = this.addTestForm.get('name').value;
        this.newTest.Description = this.addTestForm.get('description').value;
        this.newTest.QuestionTimeLimit = this.timeParser(this.addTestForm.get('hours').value, this.addTestForm.get('minutes').value, this.addTestForm.get('seconds').value);
        console.log(this.newTest.QuestionTimeLimit);
        this._testService.addTest(this.newTest)
            .subscribe(function (item) {
            _this.tests.push(item);
            console.log(item);
        });
    };
    TestsComponent.prototype.changeRoute = function (test) {
        this._testService.currentTest = test;
        this.router.navigate(['/testpage']);
    };
    TestsComponent.prototype.timeFormatConverter = function (value) {
        if (value < 10) {
            return '0' + value.toString();
        }
        else {
            return value.toString();
        }
    };
    TestsComponent.prototype.timeParser = function (hours, minutes, seconds) {
        var time = '';
        time = this.timeFormatConverter(hours) + ':' + this.timeFormatConverter(minutes) + ':' + this.timeFormatConverter(seconds);
        return time;
    };
    return TestsComponent;
}());
TestsComponent = __decorate([
    core_1.Component({
        selector: "tests",
        moduleId: module.id,
        templateUrl: 'tests.component.html',
        styleUrls: ['tests.component.css'],
    }),
    __metadata("design:paramtypes", [test_service_1.TestService, forms_1.FormBuilder, router_1.Router])
], TestsComponent);
exports.TestsComponent = TestsComponent;
//# sourceMappingURL=tests.component.js.map