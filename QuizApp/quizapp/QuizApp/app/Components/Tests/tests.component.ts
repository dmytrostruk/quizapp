﻿import { Component, OnInit, Input, Output } from "@angular/core";
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import { TestService } from '../../Services/test.service';
import { Test } from '../../Models/test.model';

@Component({
    selector: "tests",
    moduleId: module.id,
    templateUrl: 'tests.component.html',
    styleUrls: ['tests.component.css'],
})

export class TestsComponent implements OnInit {
    private tests: Test[];
    private Name;
    private Description;
    private selectedTest: Test;
    private addTestForm: FormGroup = this.formBuilder.group({
        name: ['', Validators.required],
        description: ['', Validators.required],
        hours: [0, Validators.required],
        minutes: [0, Validators.required],
        seconds: [0, Validators.required]
    })

    private newTest: Test = {
        Guid: "",
        Name: "",
        Description: "",
        TestTimeLimit: "00:00:00",
        QuestionTimeLimit: "00:00:00"
    };

    constructor(private _testService: TestService, private formBuilder: FormBuilder, private router: Router) { }

    ngOnInit() {
        this.getTests();
    }

    getTests() {
        this._testService.getTests().subscribe(tests => {
            this.tests = tests;
        });
    }

    addTest() {
        this.newTest.Name = this.addTestForm.get('name').value;
        this.newTest.Description = this.addTestForm.get('description').value;
        this.newTest.QuestionTimeLimit = this.timeParser(
            this.addTestForm.get('hours').value,
            this.addTestForm.get('minutes').value,
            this.addTestForm.get('seconds').value
        );

        console.log(this.newTest.QuestionTimeLimit);

        this._testService.addTest(this.newTest)
            .subscribe(item => {
                this.tests.push(item);
                console.log(item);
            });
    }

    changeRoute(test: Test) {
        this._testService.currentTest = test;
        this.router.navigate(['/testpage']);
    }

    timeFormatConverter(value): string {
        if (value < 10) {
            return '0' + value.toString();
        }
        else {
            return value.toString();
        }
    }

    timeParser(hours, minutes, seconds): string {
        let time: string = '';
        time = this.timeFormatConverter(hours) + ':' + this.timeFormatConverter(minutes) + ':' + this.timeFormatConverter(seconds);
        return time;
    }
}