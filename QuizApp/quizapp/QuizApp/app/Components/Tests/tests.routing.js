"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var test_component_1 = require("../Test/test.component");
var tests_component_1 = require("./tests.component");
exports.testRoutes = [
    { path: 'tests', component: tests_component_1.TestsComponent },
    { path: 'testpage', component: test_component_1.TestComponent, pathMatch: 'prefix' }
];
//# sourceMappingURL=tests.routing.js.map