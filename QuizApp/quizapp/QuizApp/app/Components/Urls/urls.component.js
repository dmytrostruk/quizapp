"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var urls_service_1 = require("../../Services/urls.service");
var test_service_1 = require("../../Services/test.service");
var UrlsComponent = (function () {
    function UrlsComponent(formBuilder, _urlsService, _testService, router, urlSerializer, location) {
        this.formBuilder = formBuilder;
        this._urlsService = _urlsService;
        this._testService = _testService;
        this.router = router;
        this.urlSerializer = urlSerializer;
        this.location = location;
        this.addUrlForm = this.formBuilder.group({
            interviewee: '',
            testGuid: ['', forms_1.Validators.required],
            numberOfRuns: [0, forms_1.Validators.required],
            startDate: ['', forms_1.Validators.required],
            startTime: ['', forms_1.Validators.required],
            endDate: ['', forms_1.Validators.required],
            endTime: ['', forms_1.Validators.required]
        });
        this.newUrl = {
            Interviewee: '',
            AllowedStartDate: '',
            AllowedEndDate: '',
            NumberOfRuns: 0,
            Guid: '',
            TestName: '',
            TestGuid: '',
            UrlInstance: ''
        };
    }
    UrlsComponent.prototype.ngOnInit = function () {
        this.getAllTestingUrls();
    };
    UrlsComponent.prototype.getAllTestingUrls = function () {
        var _this = this;
        this._urlsService.getTestingUrls()
            .subscribe(function (urls) {
            _this.testingUrls = urls;
            for (var _i = 0, _a = _this.testingUrls; _i < _a.length; _i++) {
                var testingUrl = _a[_i];
                testingUrl.UrlInstance = _this.createUrl(testingUrl.Guid);
            }
            _this.getTests();
        });
    };
    UrlsComponent.prototype.createUrl = function (testingUrlGuid) {
        var tree = this.router.createUrlTree(['/quiz', testingUrlGuid]);
        var url = this.urlSerializer.serialize(tree);
        return window.location.origin + this.location.prepareExternalUrl(url);
    };
    UrlsComponent.prototype.getTests = function () {
        var _this = this;
        this._testService.getTests()
            .subscribe(function (tests) {
            _this.tests = tests;
        });
    };
    UrlsComponent.prototype.addUrl = function () {
        var _this = this;
        this.newUrl.TestGuid = this.addUrlForm.get('testGuid').value;
        this.newUrl.Interviewee = this.addUrlForm.get('interviewee').value;
        this.newUrl.AllowedStartDate = this.addUrlForm.get('startDate').value + ' ' + this.addUrlForm.get('startTime').value;
        this.newUrl.AllowedEndDate = this.addUrlForm.get('endDate').value + ' ' + this.addUrlForm.get('endTime').value;
        this.newUrl.NumberOfRuns = this.addUrlForm.get('numberOfRuns').value;
        this._urlsService.addTestingUrl(this.newUrl)
            .subscribe(function (url) {
            url.UrlInstance = _this.createUrl(url.Guid);
            _this.testingUrls.push(url);
        });
    };
    UrlsComponent.prototype.selectUrl = function (url, index) {
        this.selectedUrl = url;
        this.selectedRow = index;
    };
    UrlsComponent.prototype.removeUrl = function () {
        var _this = this;
        this._urlsService.removeTestingUrl(this.selectedUrl.Guid)
            .subscribe(function (urlGuid) {
            var index = _this.testingUrls.indexOf(_this.selectedUrl, 0);
            if (index > -1) {
                _this.testingUrls.splice(index, 1);
            }
            _this.selectedUrl = null;
            _this.selectedRow = -1;
        });
    };
    return UrlsComponent;
}());
UrlsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "urls",
        templateUrl: 'urls.component.html',
        styleUrls: ['urls.component.css'],
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder,
        urls_service_1.UrlsService,
        test_service_1.TestService,
        router_1.Router,
        router_1.UrlSerializer,
        common_1.Location])
], UrlsComponent);
exports.UrlsComponent = UrlsComponent;
//# sourceMappingURL=urls.component.js.map