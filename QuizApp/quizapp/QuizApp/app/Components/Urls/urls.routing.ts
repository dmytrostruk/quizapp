﻿import { Routes } from '@angular/router';

import { UrlsComponent } from './urls.component';

export const urlsRoutes: Routes = [
    { path: 'urls', component: UrlsComponent },
];