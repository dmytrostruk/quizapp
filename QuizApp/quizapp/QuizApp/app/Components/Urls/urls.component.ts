﻿import { Component, OnInit } from "@angular/core";
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, UrlSerializer, UrlTree } from "@angular/router";
import { Location } from '@angular/common';

import { Test } from '../../Models/test.model';
import { TestingUrl } from '../../Models/testing-url.model';

import { UrlsService } from '../../Services/urls.service';
import { TestService } from '../../Services/test.service';

@Component({
    moduleId: module.id,
    selector: "urls",
    templateUrl: 'urls.component.html',
    styleUrls: ['urls.component.css'],
})

export class UrlsComponent implements OnInit {

    private tests: Test[];
    private selectedUrl: TestingUrl;
    private testingUrls: TestingUrl[];

    private selectedRow: number;

    private addUrlForm: FormGroup = this.formBuilder.group({
        interviewee: '',
        testGuid: ['', Validators.required],
        numberOfRuns: [0, Validators.required],
        startDate: ['', Validators.required],
        startTime: ['', Validators.required],
        endDate: ['', Validators.required],
        endTime: ['', Validators.required]
    })

    private newUrl: TestingUrl = {
        Interviewee: '',
        AllowedStartDate: '',
        AllowedEndDate: '',
        NumberOfRuns: 0,
        Guid: '',
        TestName: '',
        TestGuid: '',
        UrlInstance: ''
    }

    constructor(
        private formBuilder: FormBuilder,
        private _urlsService: UrlsService,
        private _testService: TestService,
        private router: Router,
        private urlSerializer: UrlSerializer,
        private location: Location
    ) { }

    ngOnInit() {
        this.getAllTestingUrls();
    }

    getAllTestingUrls() {
        this._urlsService.getTestingUrls()
            .subscribe(urls => {
                this.testingUrls = urls;
                for (let testingUrl of this.testingUrls) {
                    testingUrl.UrlInstance = this.createUrl(testingUrl.Guid);
                }

                this.getTests();
            });
    }

    createUrl(testingUrlGuid: string): string {
        let tree = this.router.createUrlTree(['/quiz', testingUrlGuid]);
        let url = this.urlSerializer.serialize(tree);
        return window.location.origin + this.location.prepareExternalUrl(url);
    }

    getTests() {
        this._testService.getTests()
            .subscribe(tests => {
                this.tests = tests;
            });
    }

    addUrl() {
        this.newUrl.TestGuid = this.addUrlForm.get('testGuid').value;
        this.newUrl.Interviewee = this.addUrlForm.get('interviewee').value;
        this.newUrl.AllowedStartDate = this.addUrlForm.get('startDate').value + ' ' +  this.addUrlForm.get('startTime').value;
        this.newUrl.AllowedEndDate = this.addUrlForm.get('endDate').value + ' ' + this.addUrlForm.get('endTime').value;
        this.newUrl.NumberOfRuns = this.addUrlForm.get('numberOfRuns').value;

        this._urlsService.addTestingUrl(this.newUrl)
            .subscribe(url => {
                url.UrlInstance = this.createUrl(url.Guid);
                this.testingUrls.push(url);
            });
    }

    selectUrl(url: TestingUrl, index: number) {
        this.selectedUrl = url;
        this.selectedRow = index;
    }

    removeUrl() {
        this._urlsService.removeTestingUrl(this.selectedUrl.Guid)
            .subscribe(urlGuid => {
                let index = this.testingUrls.indexOf(this.selectedUrl, 0);
                if (index > -1) {
                    this.testingUrls.splice(index, 1);
                }

                this.selectedUrl = null;
                this.selectedRow = -1;
            });
    }
}