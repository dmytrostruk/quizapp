﻿import { Component, OnInit, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id,
    selector: "error",
    templateUrl: 'error.component.html',
    styleUrls: ['error.component.css'],
    providers: []
})

export class ErrorComponent implements OnInit {

    private errorMessage: string;

    constructor(
        private elementRef: ElementRef
    ) { }

    ngOnInit() {
        this.errorMessage = this.elementRef.nativeElement.getAttribute('error-message');
    }
}