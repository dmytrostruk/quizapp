"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var test_service_1 = require("../../Services/test.service");
var quiz_service_1 = require("../../Services/quiz.service");
var UserTestComponent = (function () {
    function UserTestComponent(elementRef, _quizService) {
        this.elementRef = elementRef;
        this._quizService = _quizService;
    }
    UserTestComponent.prototype.ngOnInit = function () {
        this._quizService.testUrlGuid = this.elementRef.nativeElement.getAttribute('guid');
        this._quizService.interviewee = this.elementRef.nativeElement.getAttribute('interviewee');
    };
    return UserTestComponent;
}());
UserTestComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "usertest",
        templateUrl: 'usertest.component.html',
        providers: [
            test_service_1.TestService,
            quiz_service_1.QuizService
        ]
    }),
    __metadata("design:paramtypes", [core_1.ElementRef,
        quiz_service_1.QuizService])
], UserTestComponent);
exports.UserTestComponent = UserTestComponent;
//# sourceMappingURL=usertest.component.js.map