﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { UserTestComponent } from './usertest.component';
import { QuizComponent } from './Components/quiz.component';
import { routing } from './usertest.routing';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing
    ],
    declarations: [
        UserTestComponent,
        QuizComponent
    ],
    bootstrap: [
        UserTestComponent
    ]
})

export class UserTestModule { }