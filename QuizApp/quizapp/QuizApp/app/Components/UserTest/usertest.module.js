"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var forms_2 = require("@angular/forms");
var http_1 = require("@angular/http");
var usertest_component_1 = require("./usertest.component");
var quiz_component_1 = require("./Components/quiz.component");
var usertest_routing_1 = require("./usertest.routing");
var UserTestModule = (function () {
    function UserTestModule() {
    }
    return UserTestModule;
}());
UserTestModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            forms_2.ReactiveFormsModule,
            http_1.HttpModule,
            usertest_routing_1.routing
        ],
        declarations: [
            usertest_component_1.UserTestComponent,
            quiz_component_1.QuizComponent
        ],
        bootstrap: [
            usertest_component_1.UserTestComponent
        ]
    })
], UserTestModule);
exports.UserTestModule = UserTestModule;
//# sourceMappingURL=usertest.module.js.map