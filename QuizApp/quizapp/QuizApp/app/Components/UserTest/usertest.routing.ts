﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserTestComponent } from './usertest.component';
import { QuizComponent } from './Components/quiz.component';

const usertestRoutes: Routes = [
    { path: '', redirectTo: '/quiz/:guid', pathMatch: 'full' },
    { path: 'quiz/:guid', component: QuizComponent },
    { path: 'usertest/:guid', component: UserTestComponent },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(usertestRoutes);