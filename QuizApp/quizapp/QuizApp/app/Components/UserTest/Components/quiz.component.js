"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var quiz_service_1 = require("../../../Services/quiz.service");
var QuizComponent = (function () {
    function QuizComponent(_quizService, formBuilder) {
        this._quizService = _quizService;
        this.formBuilder = formBuilder;
        this.iterator = 0;
        this.startFlag = true;
        this.testFlag = false;
        this.finishFlag = false;
        this.quizModel = {
            AttemptGuid: '',
            TestingGuid: '',
            Interviewee: '',
            TestingStartDateTime: '',
            TestingEndDateTime: '',
            Questions: []
        };
        this.intervieweeForm = this.formBuilder.group({
            interviewee: ['', forms_1.Validators.required]
        });
        this.radioForm = this.formBuilder.group({
            optradio: ['', forms_1.Validators.required]
        });
    }
    QuizComponent.prototype.ngOnInit = function () {
        this.testUrlGuid = this._quizService.testUrlGuid;
        this.interviewee = this._quizService.interviewee;
        if (this.isNameExist()) {
            this.intervieweeForm.setValue({ interviewee: this.interviewee });
            this.intervieweeForm.get('interviewee').disable();
        }
    };
    QuizComponent.prototype.isNameExist = function () {
        if (this.interviewee === null || this.interviewee === '') {
            return false;
        }
        else {
            return true;
        }
    };
    QuizComponent.prototype.getInfoAndStartTest = function () {
        var _this = this;
        this._quizService.getInfoAndStartTest(this.testUrlGuid)
            .subscribe(function (test) {
            _this.currentTest = test;
            if (_this.currentTest != null) {
                _this.startFlag = false;
                _this.testFlag = true;
                _this.quizModel.AttemptGuid = _this.currentTest.AttemptGuid;
                _this.quizModel.TestingGuid = _this.testUrlGuid;
                _this.quizModel.Interviewee = _this.intervieweeForm.get('interviewee').value;
                _this.quizModel.TestingStartDateTime = _this.getCurrentDateTime();
                _this.resetTimer();
            }
            _this.currentQuestion = _this.currentTest.Questions[_this.iterator];
        });
    };
    QuizComponent.prototype.nextQuestion = function () {
        if (this.currentQuestion.Answers.find(function (answer) { return answer.IsSelected === true; }) != undefined) {
            this.currentQuestion.Answered = true;
        }
        this.iterator++;
        if (this.iterator == this.currentTest.Questions.length) {
            this.testFlag = false;
            this.finishFlag = true;
            clearInterval(this.timer);
            this.finishTest();
        }
        else {
            this.currentQuestion = this.currentTest.Questions[this.iterator];
            this.resetTimer();
        }
    };
    QuizComponent.prototype.selectRadioAnswer = function (selectedAnswer) {
        var previousSelected = this.currentQuestion.Answers.find(function (answer) { return answer.IsSelected == true; });
        if (previousSelected != undefined) {
            previousSelected.IsSelected = false;
        }
        selectedAnswer.IsSelected = true;
    };
    QuizComponent.prototype.selectCheckboxAnswer = function (selectedAnswer) {
        if (selectedAnswer.IsSelected === false) {
            selectedAnswer.IsSelected = true;
        }
        else {
            selectedAnswer.IsSelected = false;
        }
    };
    QuizComponent.prototype.finishTest = function () {
        this.quizModel.TestingEndDateTime = this.getCurrentDateTime();
        for (var _i = 0, _a = this.currentTest.Questions; _i < _a.length; _i++) {
            var question = _a[_i];
            var questionModel = { QuestionGuid: question.Guid, AnswersSelected: [] };
            for (var _b = 0, _c = question.Answers; _b < _c.length; _b++) {
                var answer = _c[_b];
                if (answer.IsSelected == true) {
                    questionModel.AnswersSelected.push(answer.Guid);
                }
            }
            this.quizModel.Questions.push(questionModel);
        }
        this._quizService.finishTest(this.quizModel).subscribe();
    };
    QuizComponent.prototype.getCurrentDateTime = function () {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;
        return dateTime;
    };
    QuizComponent.prototype.resetTimer = function () {
        var _this = this;
        clearInterval(this.timer);
        this.hours = this.currentTest.QuestionTimeLimit.Hours;
        this.minutes = this.currentTest.QuestionTimeLimit.Minutes;
        this.seconds = this.currentTest.QuestionTimeLimit.Seconds;
        this.time = this.timeParser(this.hours, this.minutes, this.seconds);
        this.timer = setInterval(function () { return _this.myTimer(); }, 1000);
    };
    QuizComponent.prototype.myTimer = function () {
        this.time = this.timeParser(this.hours, this.minutes, this.seconds);
        if (this.seconds <= 0 && this.minutes <= 0 && this.hours <= 0) {
            this.nextQuestion();
        }
        if (this.seconds == 0) {
            this.minutes--;
            if (this.minutes < 0) {
                this.hours--;
                this.minutes = 59;
            }
            this.seconds = 59;
        }
        else {
            this.seconds--;
        }
    };
    QuizComponent.prototype.timeFormatConverter = function (value) {
        if (value < 10) {
            return '0' + value.toString();
        }
        else {
            return value.toString();
        }
    };
    QuizComponent.prototype.timeParser = function (hours, minutes, seconds) {
        var time = '';
        time = this.timeFormatConverter(hours) + ':' + this.timeFormatConverter(minutes) + ':' + this.timeFormatConverter(seconds);
        return time;
    };
    return QuizComponent;
}());
QuizComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "quiz",
        templateUrl: 'quiz.component.html',
        styleUrls: ['quiz.component.css'],
    }),
    __metadata("design:paramtypes", [quiz_service_1.QuizService,
        forms_1.FormBuilder])
], QuizComponent);
exports.QuizComponent = QuizComponent;
//# sourceMappingURL=quiz.component.js.map