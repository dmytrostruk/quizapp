﻿import { Component, OnInit } from "@angular/core";
import { NgForm, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { QuizService } from '../../../Services/quiz.service';
import { Quiz } from '../../../Models/quiz.model';
import { QuizQuestion } from '../../../Models/quiz-question.model';

@Component({
    moduleId: module.id,
    selector: "quiz",
    templateUrl: 'quiz.component.html',
    styleUrls: ['quiz.component.css'],
})

export class QuizComponent implements OnInit {

    private testUrlGuid: string;
    private interviewee: string;

    private currentTest: any;
    private currentQuestion: any;
    private iterator: number = 0;

    private startFlag: boolean = true;
    private testFlag: boolean = false;
    private finishFlag: boolean = false;

    private quizModel: Quiz = {
        AttemptGuid: '',
        TestingGuid: '',
        Interviewee: '',
        TestingStartDateTime: '',
        TestingEndDateTime: '',
        Questions: []
    }

    private hours: number;
    private minutes: number;
    private seconds: number;
    private time: string;
    private timer: any;

    constructor(
        private _quizService: QuizService,
        private formBuilder: FormBuilder
    ) { }

    private intervieweeForm: FormGroup = this.formBuilder.group({
        interviewee: ['', Validators.required]
    })

    private radioForm: FormGroup = this.formBuilder.group({
        optradio: ['', Validators.required]
    })

    ngOnInit() {
        this.testUrlGuid = this._quizService.testUrlGuid;
        this.interviewee = this._quizService.interviewee;

        if (this.isNameExist()) {
            this.intervieweeForm.setValue({ interviewee: this.interviewee });
            this.intervieweeForm.get('interviewee').disable();
        }
    }

    isNameExist(): boolean {
        if (this.interviewee === null || this.interviewee === '') {
            return false;
        }
        else {
            return true;
        }
    }

    getInfoAndStartTest() {
        this._quizService.getInfoAndStartTest(this.testUrlGuid)
            .subscribe(test => {
                this.currentTest = test;
                if (this.currentTest != null) {
                    this.startFlag = false;
                    this.testFlag = true;

                    this.quizModel.AttemptGuid = this.currentTest.AttemptGuid;
                    this.quizModel.TestingGuid = this.testUrlGuid;
                    this.quizModel.Interviewee = this.intervieweeForm.get('interviewee').value;
                    this.quizModel.TestingStartDateTime = this.getCurrentDateTime();

                    this.resetTimer();
                }
                this.currentQuestion = this.currentTest.Questions[this.iterator];
            });
    }

    nextQuestion() {
        if (this.currentQuestion.Answers.find(answer => answer.IsSelected === true) != undefined) {
            this.currentQuestion.Answered = true;
        }

        this.iterator++;
        if (this.iterator == this.currentTest.Questions.length) {
            this.testFlag = false;
            this.finishFlag = true;
            clearInterval(this.timer);
            this.finishTest();
        }
        else {
            this.currentQuestion = this.currentTest.Questions[this.iterator];
            this.resetTimer();
        }
    }

    selectRadioAnswer(selectedAnswer) {
        let previousSelected = this.currentQuestion.Answers.find(answer => answer.IsSelected == true);
        if (previousSelected != undefined) {
            previousSelected.IsSelected = false;
        }
        selectedAnswer.IsSelected = true;
    }

    selectCheckboxAnswer(selectedAnswer) {
        if (selectedAnswer.IsSelected === false) {
            selectedAnswer.IsSelected = true;
        }
        else {
            selectedAnswer.IsSelected = false;
        }
    }

    finishTest() {
        this.quizModel.TestingEndDateTime = this.getCurrentDateTime();
        for (let question of this.currentTest.Questions) {

            let questionModel = { QuestionGuid: question.Guid, AnswersSelected: [] };

            for (let answer of question.Answers) {
                if (answer.IsSelected == true) {
                    questionModel.AnswersSelected.push(answer.Guid);
                }
            }

            this.quizModel.Questions.push(questionModel);
        }

        this._quizService.finishTest(this.quizModel).subscribe();
    }

    getCurrentDateTime(): string {
        var today = new Date();
        var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date + ' ' + time;

        return dateTime;
    }

    resetTimer() {
        clearInterval(this.timer);
 
        this.hours = this.currentTest.QuestionTimeLimit.Hours;
        this.minutes = this.currentTest.QuestionTimeLimit.Minutes;
        this.seconds = this.currentTest.QuestionTimeLimit.Seconds;
        this.time = this.timeParser(this.hours, this.minutes, this.seconds);

        this.timer = setInterval(() => this.myTimer(), 1000);
    }

    myTimer() {

        this.time = this.timeParser(this.hours, this.minutes, this.seconds);

        if (this.seconds <= 0 && this.minutes <= 0 && this.hours <= 0) {
            this.nextQuestion();
        }

        if (this.seconds == 0) {
            this.minutes--;

            if (this.minutes < 0) {
                this.hours--;
                this.minutes = 59;
            }

            this.seconds = 59;
        }
        else {
            this.seconds--;
        }
    }

    timeFormatConverter(value): string {
        if (value < 10) {
            return '0' + value.toString();
        }
        else {
            return value.toString();
        }
    }

    timeParser(hours, minutes, seconds): string {
        let time: string = '';
        time = this.timeFormatConverter(hours) + ':' + this.timeFormatConverter(minutes) + ':' + this.timeFormatConverter(seconds);
        return time;
    }
}