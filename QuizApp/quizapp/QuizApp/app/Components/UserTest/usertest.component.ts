﻿import { Component, OnInit, ElementRef } from "@angular/core";
import { Router, ActivatedRoute } from '@angular/router';

import { TestService } from '../../Services/test.service';
import { QuizService } from '../../Services/quiz.service';

@Component({
    moduleId: module.id,
    selector: "usertest",
    templateUrl: 'usertest.component.html',
    providers: [
        TestService,
        QuizService
    ]
})

export class UserTestComponent implements OnInit {

    constructor(
        private elementRef: ElementRef,
        private _quizService: QuizService
    ) { }

    ngOnInit() {
        this._quizService.testUrlGuid = this.elementRef.nativeElement.getAttribute('guid');
        this._quizService.interviewee = this.elementRef.nativeElement.getAttribute('interviewee');
    }
}