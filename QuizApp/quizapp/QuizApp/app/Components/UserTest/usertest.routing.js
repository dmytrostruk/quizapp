"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var usertest_component_1 = require("./usertest.component");
var quiz_component_1 = require("./Components/quiz.component");
var usertestRoutes = [
    { path: '', redirectTo: '/quiz/:guid', pathMatch: 'full' },
    { path: 'quiz/:guid', component: quiz_component_1.QuizComponent },
    { path: 'usertest/:guid', component: usertest_component_1.UserTestComponent },
];
exports.routing = router_1.RouterModule.forRoot(usertestRoutes);
//# sourceMappingURL=usertest.routing.js.map