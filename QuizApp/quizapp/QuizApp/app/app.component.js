"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var test_service_1 = require("./Services/test.service");
var question_service_1 = require("./Services/question.service");
var answer_service_1 = require("./Services/answer.service");
var urls_service_1 = require("./Services/urls.service");
var results_service_1 = require("./Services/results.service");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent.prototype.logOut = function () {
        window.location.href = 'Account/LogOff';
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "my-app",
        templateUrl: 'app.component.html',
        providers: [
            test_service_1.TestService,
            question_service_1.QuestionService,
            answer_service_1.AnswerService,
            urls_service_1.UrlsService,
            results_service_1.ResultsService
        ]
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map