﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Result } from '../Models/result.model';

@Injectable()
export class ResultsService {

    constructor(private _http: Http) {

    }

    getResults(): Observable<Result[]> {
        return this._http.get('Admin/GetAllTestingResults')
            .map(res => <Result[]>res.json());
    }

    removeTestingUrl(resultItemGuid: string): Observable<string> {
        let bodyString = JSON.stringify({
            testingResultGuid: resultItemGuid
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/RemoveTestingResult', bodyString, options)
            .map((res: Response) => <string>res.json());
    }
}