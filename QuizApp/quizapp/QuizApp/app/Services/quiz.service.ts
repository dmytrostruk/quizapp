﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';


@Injectable()
export class QuizService {
    public testUrlGuid: string;
    public interviewee: string;

    constructor(private _http: Http) {

    }

    getInfoAndStartTest(testingUrlGuid: string) {
        let params: URLSearchParams = new URLSearchParams();
        params.set('testingUrlGuid', testingUrlGuid)

        return this._http.get('Quiz/GetInfoAndStartTest', { search: params })
            .map(res => res.json());
    }

    finishTest(passedTest) {
        let bodyString = JSON.stringify({
            testPassing: passedTest,
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Quiz/FinishTest', bodyString, options)
            .map((res: Response) => res.json());
    }
}