﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Answer } from '../Models/answer.model';

@Injectable()
export class AnswerService {
    
    constructor(private _http: Http) {

    }

    addNewAnswer(questionItemGuid: string, newAnswer: Answer): Observable<Answer> {
        let bodyString = JSON.stringify({
            questionGuid: questionItemGuid,
            answer: newAnswer
        });

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/CreateAnswer', bodyString, options)
            .map((res: Response) => <Answer>res.json());
    }

    deleteAnswer(answerItemGuid: string): Observable<string> {
        let bodyString = JSON.stringify({
            answerGuid: answerItemGuid,
        });

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/RemoveAnswer', bodyString, options)
            .map((res: Response) => <string>res.json());
    }
}