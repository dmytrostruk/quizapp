﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Question } from '../Models/question.model';
import { AnswerService } from '../Services/answer.service';

@Injectable()
export class QuestionService {
    
    constructor(private _http: Http) {

    }

    getQuestionsByTestGuid(testItemGuid: string): Observable<Question[]> {

        let params: URLSearchParams = new URLSearchParams();
        params.set('testGuid', testItemGuid)

        return this._http.get('Apilike/GetQuestionsByTestGuid', { search: params })
            .map(res => res.json());
    }

    addQuestion(testItemGuid: string, newQuestion: Question) : Observable<Question> {
        let bodyString = JSON.stringify({
            testGuid: testItemGuid,
            question: newQuestion
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/CreateQuestion', bodyString, options)
            .map((res: Response) => <Question>res.json());
    }

    removeQuestion(testItemGuid: string, questionItemGuid: string): Observable<string> {
        let bodyString = JSON.stringify({
            testGuid: testItemGuid,
            questionGuid: questionItemGuid
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/RemoveQuestion', bodyString, options)
            .map((res: Response) => <string>res.json());
    }

    editQuestion(questionItemGuid: string, updatedQuestion: Question): Observable<Question> {
        let bodyString = JSON.stringify({
            questionGuid: questionItemGuid,
            question: updatedQuestion
        });

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/UpdateQuestion', bodyString, options)
            .map((res: Response) => <Question>res.json());
    }
}