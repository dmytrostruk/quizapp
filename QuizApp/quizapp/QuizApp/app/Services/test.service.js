"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/Rx");
require("rxjs/add/operator/map");
var TestService = (function () {
    function TestService(_http) {
        this._http = _http;
    }
    TestService.prototype.getTests = function () {
        return this._http.get('Admin/GetAllTests')
            .map(function (res) { return res.json(); });
    };
    TestService.prototype.addTest = function (newTest) {
        var bodyString = JSON.stringify(newTest);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post('Apilike/CreateTest', bodyString, options)
            .map(function (res) { return res.json(); });
    };
    TestService.prototype.removeTest = function (testItemGuid) {
        var bodyString = JSON.stringify({
            testGuid: testItemGuid
        });
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post('Apilike/RemoveTest', bodyString, options)
            .map(function (res) { return res.json(); });
    };
    TestService.prototype.editTest = function (testItemGuid, updatedTest) {
        var bodyString = JSON.stringify({
            testGuid: testItemGuid,
            test: updatedTest
        });
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post('Apilike/UpdateTest', bodyString, options)
            .map(function (res) { return res.json(); });
    };
    return TestService;
}());
TestService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], TestService);
exports.TestService = TestService;
//# sourceMappingURL=test.service.js.map