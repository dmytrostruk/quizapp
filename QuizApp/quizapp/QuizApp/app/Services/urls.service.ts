﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { TestingUrl } from '../Models/testing-url.model';

@Injectable()
export class UrlsService {

    constructor(private _http: Http) {

    }

    getTestingUrls(): Observable<TestingUrl[]> {
        return this._http.get('Admin/GetAllTestingUrls')
            .map(res => res.json());
    }

    addTestingUrl(newTestingUrl: TestingUrl): Observable<TestingUrl> {
        let bodyString = JSON.stringify(newTestingUrl);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/CreateTestingUrl', bodyString, options)
            .map((res: Response) => <TestingUrl>res.json());
    }

    removeTestingUrl(urlItemGuid: string): Observable<string> {
        let bodyString = JSON.stringify({
            testingUrlGuid: urlItemGuid
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/RemoveTestingUrl', bodyString, options)
            .map((res: Response) => <string>res.json());
    }
}