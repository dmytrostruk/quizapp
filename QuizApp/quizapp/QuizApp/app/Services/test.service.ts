﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Test } from '../Models/test.model';

@Injectable()
export class TestService {
    public currentTest: Test;

    constructor(private _http: Http) {
        
    }

    getTests(): Observable<Test[]> {
        return this._http.get('Admin/GetAllTests')
            .map(res => res.json());
    }

    addTest(newTest: Test): Observable<Test> {
        let bodyString = JSON.stringify(newTest);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/CreateTest', bodyString, options)
            .map((res: Response) => <Test>res.json());
    }

    removeTest(testItemGuid: string): Observable<string> {
        let bodyString = JSON.stringify({
            testGuid: testItemGuid
        });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/RemoveTest', bodyString, options)
            .map((res: Response) => <string>res.json());
    }

    editTest(testItemGuid: string, updatedTest: Test): Observable<Test> {
        let bodyString = JSON.stringify({
            testGuid: testItemGuid,
            test: updatedTest
        });

        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this._http.post('Apilike/UpdateTest', bodyString, options)
            .map((res: Response) => <Test>res.json());
    }
}