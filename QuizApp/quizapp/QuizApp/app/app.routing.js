"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var results_component_1 = require("./Components/Results/results.component");
var tests_component_1 = require("./Components/Tests/tests.component");
var urls_component_1 = require("./Components/Urls/urls.component");
var tests_routing_1 = require("./Components/Tests/tests.routing");
var urls_routing_1 = require("./Components/Urls/urls.routing");
var appRoutes = [
    { path: '', redirectTo: '/tests', pathMatch: 'full' },
    { path: 'tests', component: tests_component_1.TestsComponent, },
    { path: 'urls', component: urls_component_1.UrlsComponent },
    { path: 'results', component: results_component_1.ResultsComponent }
].concat(tests_routing_1.testRoutes, urls_routing_1.urlsRoutes);
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map