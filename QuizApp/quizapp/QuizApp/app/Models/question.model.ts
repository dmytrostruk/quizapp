﻿import { Answer } from './answer.model';

export interface Question {
    Guid: string,
    Instance: string,
    Hint: string,
    Answers: Answer[]
}