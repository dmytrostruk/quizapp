﻿export interface QuizQuestion{
    QuestionGuid: string,
    AnswersSelected: string[]
}