﻿import { QuizQuestion } from './quiz-question.model';

export interface Quiz {
    AttemptGuid: string,
    TestingGuid: string,
    Interviewee: string,
    TestingStartDateTime: string,
    TestingEndDateTime: string,
    Questions: QuizQuestion[]
}