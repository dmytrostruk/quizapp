﻿export interface Result {
    Guid: string,
    TestingStartDateTime: string,
    TestingEndDateTime: string,
    Duration: string,
    Interviewee: string,
    TestName: string,
    Score: number
}