﻿export interface Test {
    Guid: string,
    Name: string,
    Description: string,
    TestTimeLimit: string,
    QuestionTimeLimit: string,
}