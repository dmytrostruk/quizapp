﻿export interface TestingUrl {
    Interviewee: string,
    AllowedStartDate: string,
    AllowedEndDate: string,
    NumberOfRuns: number,
    Guid: string,
    TestName: string,
    TestGuid: string,
    UrlInstance: string
}