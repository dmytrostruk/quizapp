﻿import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { ErrorModule } from './Components/Error/error.module';
const platform = platformBrowserDynamic();
platform.bootstrapModule(ErrorModule);